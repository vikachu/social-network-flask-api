from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app.main import web_app, db
from app.models import User, Post, Like


migrate = Migrate(web_app, db)
manager = Manager(web_app)

manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
