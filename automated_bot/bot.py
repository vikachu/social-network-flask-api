import os
import yaml
import random
import requests

# BASE_URL = "http://localhost:8080/api/v1"
BASE_URL = os.getenv('BASE_URL')


def sign_up_users(number_of_users):
    users = []
    for i in range(number_of_users):
        data = {
            "full_name": f"user_{i}",
            "avatar_url": "",
            "email": f"email{i}@gmail.com",
            "password": f"password{i}"
        }
        response = requests.post(url=f'{BASE_URL}/auth/signup', json=data)
        users.append(response.json())
    return users


def create_posts(users, max_posts_per_user):
    posts = []
    for user in users:
        user_id = user.get('user').get('user_id')
        access_token = user.get('access_token')

        for i in range(random.randint(1, max_posts_per_user)):
            data = {
                "post_text": "lol-top-kek",
                "post_image": "",
                "user_id": user_id
            }
            headers = {'Authorization': f'Bearer {access_token}'}
            response = requests.post(url=f'{BASE_URL}/posts', json=data, headers=headers)
            posts.append(response.json())
    return posts


def like_posts(posts, users, max_likes_per_user):
    likes = []
    post_ids = [post.get('post_id') for post in posts]
    for user in users:
        user_id = user.get('user').get('user_id')
        access_token = user.get('access_token')

        for i in range(random.randint(1, max_likes_per_user)):
            post_to_like = random.choices(post_ids)[0]  # choose random post to (un)like
            data = {
                "is_active": bool(random.getrandbits(1))
            }
            headers = {'Authorization': f'Bearer {access_token}'}
            response = requests.put(url=f'{BASE_URL}/likes/users/{user_id}/posts/{post_to_like}',
                                    json=data, headers=headers)
            likes.append(response.json())

    return likes


def start_activity(config_file_path):
    with open(config_file_path) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)

        number_of_users = data.get('number_of_users')
        max_posts_per_user = data.get('max_posts_per_user')
        max_likes_per_user = data.get('max_likes_per_user')

        users = sign_up_users(number_of_users)
        posts = create_posts(users, max_posts_per_user)
        like_posts(posts, users, max_likes_per_user)


if __name__ == "__main__":
    start_activity(config_file_path="config.yml")
