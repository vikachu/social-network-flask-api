# Social network Flask API  

## System design  
![ER-model](img/ER-model-social-network.png)

## REST API  
![REST API](img/rest-api.png)

## Getting started  

### Installing  

```
pip install -r requirements.txt
pip install -e .
```
### Database migrations  
```
python manage.py db upgrade
```
### Running  
```
python app/main.py
```

## Deployed on Heroku example  
Follow the link: https://social-network-flask-api.herokuapp.com/api/v1/docs .
