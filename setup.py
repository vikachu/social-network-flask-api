from setuptools import setup

setup(
    name="social-network-flask-api",
    version="1.0.0",
    install_requires=[],
    packages=["app"],
    url="",
    download_url="",
    description="",
    long_description="",
    license="MIT",
    keywords="",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python",
        "Natural Language :: English",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
    ],
)
