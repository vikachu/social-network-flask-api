import uuid
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID

from app.main import db


class Post(db.Model):
    __tablename__ = "post"

    post_id = db.Column(UUID(as_uuid=True), primary_key=True, default=lambda: str(uuid.uuid4()))
    post_text = db.Column(db.String(2200), nullable=False)
    post_image = db.Column(db.String(500))
    published_datetime = db.Column(db.DateTime(), nullable=False)
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('user.user_id', onupdate="CASCADE",
                        ondelete="NO ACTION"), nullable=False)

    user = db.relationship("User", back_populates="post")
    like = db.relationship("Like", back_populates="post")

    def __init__(self, post_text, post_image, user_id):
        self.post_text = post_text
        self.post_image = post_image
        self.published_datetime = datetime.now()
        self.user_id = uuid.UUID(user_id)
