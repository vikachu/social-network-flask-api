import uuid
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID

from app.main import db


class Like(db.Model):
    __tablename__ = "like"

    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('user.user_id', onupdate="CASCADE",
                        ondelete="NO ACTION"), primary_key=True, nullable=False)
    post_id = db.Column(UUID(as_uuid=True), db.ForeignKey('post.post_id', onupdate="CASCADE",
                        ondelete="NO ACTION"), primary_key=True, nullable=False)
    published_datetime = db.Column(db.DateTime(), nullable=False)
    is_active = db.Column(db.Boolean(), nullable=False)

    user = db.relationship("User", back_populates="like")
    post = db.relationship("Post", back_populates="like")

    def __init__(self, user_id, post_id, is_active):
        self.user_id = uuid.UUID(user_id)
        self.post_id = uuid.UUID(post_id)
        self.published_datetime = datetime.now()
        self.is_active = is_active
