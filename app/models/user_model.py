import uuid
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID

from app.main import db, flask_bcrypt


class User(db.Model):
    __tablename__ = "user"

    user_id = db.Column(UUID(as_uuid=True), primary_key=True, default=lambda: str(uuid.uuid4()))
    full_name = db.Column(db.String(100), nullable=False)
    avatar_url = db.Column(db.String(500))
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(200), nullable=False)
    last_login_time = db.Column(db.DateTime(), nullable=False)
    last_request_time = db.Column(db.DateTime(), nullable=False)

    post = db.relationship("Post", back_populates="user")
    like = db.relationship("Like", back_populates="user")

    def __init__(self, full_name, avatar_url, email, password):
        self.full_name = full_name
        self.avatar_url = avatar_url
        self.email = email
        self.password = flask_bcrypt.generate_password_hash(password).decode()
        self.last_login_time = datetime.now()
        self.last_request_time = datetime.now()

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password, password)
