from flask import Blueprint
from flask_restplus import Api

from app.controllers.health_controller import api_health
from app.controllers.auth_controller import api_auth
from app.controllers.user_controller import api_user
from app.controllers.post_controller import api_posts
from app.controllers.like_controller import api_likes
from app.controllers.analytics_controller import api_analytics

authorizations = {
    'Access token': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token."
    },
    'Refresh token': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token."
    },
}
blueprint = Blueprint('api', __name__)
api = Api(blueprint, doc='/docs', authorizations=authorizations)

api.add_namespace(api_health)
api.add_namespace(api_auth)
api.add_namespace(api_user)
api.add_namespace(api_posts)
api.add_namespace(api_likes)
api.add_namespace(api_analytics)
