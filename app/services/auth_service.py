from datetime import timedelta, datetime
from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity

from app.main import db
from app.models.user_model import User
from app.services.user_service import UserService


class AuthService():
    @staticmethod
    def signup_user(post_data):
        user = User.query.filter_by(email=post_data.get('email')).first()
        if not user:
            user = User(
                full_name=post_data.get('full_name'),
                avatar_url=post_data.get('avatar_url'),
                email=post_data.get('email'),
                password=post_data.get('password'),
            )
            db.session.add(user)
            db.session.commit()

            return {
                "user": {
                    "user_id": str(user.user_id),
                    "full_name": user.full_name,
                    "avatar_url": user.avatar_url,
                    "email": user.email,
                    "last_login_time": user.last_login_time.isoformat(),
                    "last_request_time": user.last_request_time.isoformat(),
                },
                "access_token": create_access_token(identity=user.email, expires_delta=timedelta(days=365)),
                "refresh_token": create_refresh_token(identity=user.email, expires_delta=timedelta(days=365))
            }, 201
        else:
            return {
                "message": "User already exists."
            }, 409

    @staticmethod
    def login_user(post_data):
        email = post_data.get('email')
        password = post_data.get('password')

        user = UserService.get_user_by_email(email)
        if not user:
            return {
                "message": "No user with such an email exists."
            }, 401

        if not user.check_password(password):
            return {
                "message": "Wrong password."
            }, 401

        # update last login and request time
        user.last_login_time = datetime.now()
        user.last_request_time = datetime.now()
        db.session.add(user)
        db.session.commit()

        return {
            "user": {
                "user_id": str(user.user_id),
                "full_name": user.full_name,
                "avatar_url": user.avatar_url,
                "email": user.email,
                "last_login_time": user.last_login_time.isoformat(),
                "last_request_time": user.last_request_time.isoformat(),
            },
            "access_token": create_access_token(identity=user.email, expires_delta=timedelta(days=365)),
            "refresh_token": create_refresh_token(identity=user.email, expires_delta=timedelta(days=365))
        }, 200

    @staticmethod
    def refresh_token():
        current_user = get_jwt_identity()
        return {
            "access_token": create_access_token(identity=current_user)
        }, 200
