from app.models.user_model import User


class UserService():
    @staticmethod
    def get_user_by_email(email):
        user = User.query.filter_by(email=email).first()
        return user

    @staticmethod
    def get_user_activity_info(user_id):
        user = User.query.filter_by(user_id=user_id).first()
        if user:
            return {
                'last_login_time': user.last_login_time.isoformat(),
                'last_request_time': user.last_request_time.isoformat()
            }, 200
        else:
            return {
                'message': 'No such user exists.'
            }, 404

    @staticmethod
    def get_all_users():
        users = User.query.all()
        return [{
            'user_id': str(user.user_id),
            'full_name': user.full_name,
            'email': user.email,
            'last_login_time': user.last_login_time.isoformat(),
            'last_request_time': user.last_request_time.isoformat()
        } for user in users], 200
