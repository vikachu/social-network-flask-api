from datetime import datetime

from app.main import db
from app.models.like_model import Like


class LikeService():
    @staticmethod
    def update_like(put_data, user_id, post_id):
        like = Like.query.filter_by(user_id=user_id, post_id=post_id).first()
        if not like:
            like = Like(
                user_id=user_id,
                post_id=post_id,
                is_active=put_data.get('is_active')
            )
            db.session.add(like)
            db.session.commit()

            return {
                "user_id": str(like.user_id),
                "post_id": str(like.post_id),
                "published_datetime": like.published_datetime.isoformat(),
                "is_active": like.is_active,
            }, 201
        else:
            like.published_datetime = datetime.now()
            like.is_active = put_data.get('is_active')

            db.session.add(like)
            db.session.commit()

            return {
                "user_id": str(like.user_id),
                "post_id": str(like.post_id),
                "published_datetime": like.published_datetime.isoformat(),
                "is_active": like.is_active,
            }, 200

    @staticmethod
    def count_number_of_likes_per_post(post_id):
        likes_number = Like.query.filter_by(post_id=post_id, is_active=True).count()
        return {
            "likes_number": likes_number,
        }, 200
