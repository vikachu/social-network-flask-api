import re
from functools import wraps
from datetime import datetime

from flask import request
from flask_jwt_extended import decode_token
from flask_jwt_extended.config import config


def last_request_time(fn):
    """Takes access token and changes last_request_time of the user.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):

        header_name = config.header_name
        auth_header = request.headers.get(header_name, None)
        token = re.sub(r'^Bearer ', '', auth_header)

        user_email = decode_token(token).get('identity')

        from app.services.user_service import UserService
        from app.main import db

        user = UserService.get_user_by_email(user_email)
        user.last_request_time = datetime.now()

        db.session.add(user)
        db.session.commit()

        return fn(*args, **kwargs)
    return wrapper
