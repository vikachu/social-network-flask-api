from app.main import db


class AnalyticsService():
    @staticmethod
    def get_likes_per_day(date_from, date_to):
        # raw sql to use PostgreSQL functions date_trunc and to_timestamp
        sql = """
        SELECT date_trunc('day', published_datetime) as d, COUNT(*)
        FROM "like"
        WHERE published_datetime >= to_timestamp(%(date_from)s, 'YYYY-MM-DD') AND
              published_datetime <= to_timestamp(%(date_to)s, 'YYYY-MM-DD') + time '23:59:59'
        GROUP BY d;
        """

        grouped_likes = db.engine.execute(sql, {'date_from': date_from, 'date_to': date_to}).fetchall()
        return [{
            "day": dict(row).get('d').strftime('%Y-%m-%d'),
            "likes_number": dict(row).get('count'),
        } for row in grouped_likes], 200
