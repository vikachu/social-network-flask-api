import uuid

from app.main import db
from app.models.post_model import Post


class PostService():
    @staticmethod
    def get_all(query_params):
        user_id = query_params.get('user_id')
        user_id = uuid.UUID(user_id) if user_id else None

        posts = Post.query.filter_by(user_id=user_id).all() if user_id is not None \
            else Post.query.all()
        return [{
            "post_id": str(post.post_id),
            "post_text": post.post_text,
            "post_image": post.post_image,
            "published_datetime": post.published_datetime.isoformat(),
            "user_id": str(post.user_id),
        } for post in posts], 200

    @staticmethod
    def create(post_data):
        post = Post(
            post_text=post_data.get('post_text'),
            post_image=post_data.get('post_image'),
            user_id=post_data.get('user_id'),
        )
        db.session.add(post)
        db.session.commit()

        return {
            "post_id": str(post.post_id),
            "post_text": post.post_text,
            "post_image": post.post_image,
            "published_datetime": post.published_datetime.isoformat(),
            "user_id": str(post.user_id),
        }, 201
