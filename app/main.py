from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager

from app.api import blueprint, api

web_app = Flask("Social network Flask API")
web_app.config.from_object('config')

web_app.register_blueprint(blueprint, url_prefix='/api/v1')

db = SQLAlchemy(web_app)
flask_bcrypt = Bcrypt(web_app)
jwt = JWTManager(web_app)
jwt._set_error_handler_callbacks(api)  # flask-restplus bug with jwt error handling

if __name__ == "__main__":
    web_app.run("0.0.0.0", 8080)
