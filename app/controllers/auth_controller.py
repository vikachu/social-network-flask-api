from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_jwt_extended import jwt_refresh_token_required


api_auth = Namespace("auth", description="User authorization requests.")

user_signup_fields = api_auth.model('Register user payload.', {
    "full_name": fields.String,
    "avatar_url": fields.String,
    "email": fields.String,
    "password": fields.String,
})


@api_auth.route("/signup")
class Signup(Resource):
    @api_auth.doc(body=user_signup_fields,
                  responses={201: 'Successfully created.', 409: 'User already exists.'})
    def post(self):
        """Signup new user.
        """
        post_data = request.get_json()
        from app.services.auth_service import AuthService
        return AuthService.signup_user(post_data)


user_login_fields = api_auth.model('Login user payload.', {
    "email": fields.String,
    "password": fields.String,
})


@api_auth.route("/login")
class Login(Resource):
    @api_auth.doc(body=user_login_fields,
                  responses={200: 'Success', 401: 'Non authorized.'})
    def post(self):
        """Login user.
        """
        post_data = request.get_json()
        from app.services.auth_service import AuthService
        return AuthService.login_user(post_data)


@api_auth.route("/token/refresh")
class RefreshToken(Resource):
    @jwt_refresh_token_required
    @api_auth.doc(security='Refresh token')
    def post(self):
        """Refresh access token.
        """
        from app.services.auth_service import AuthService
        return AuthService.refresh_token()
