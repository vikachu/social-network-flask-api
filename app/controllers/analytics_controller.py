from flask import request
from flask_restplus import Namespace, Resource
from flask_jwt_extended import jwt_required

from app.services.utils_service import last_request_time


api_analytics = Namespace("analytics", description="Analytics requests.")


@api_analytics.route("/")
class Analytics(Resource):
    @jwt_required
    @last_request_time
    @api_analytics.doc(params={'date_from': {'description':'Start date.', 'required': True},
                               'date_to': {'description':'End date.', 'required': True}},
                       security='Access token')
    def get(self):
        """Get number of likes from-to period aggregated by day.
        """
        query_params = request.args
        date_from = query_params.get('date_from')
        date_to = query_params.get('date_to')

        from app.services.analytics_service import AnalyticsService
        return AnalyticsService.get_likes_per_day(date_from, date_to)
