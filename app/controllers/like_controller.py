from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_jwt_extended import jwt_required

from app.services.utils_service import last_request_time


api_likes = Namespace("likes", description="Like requests.")

like_fields = api_likes.model('Like-unlike payload.', {
    "is_active": fields.Boolean,
})


@api_likes.route("/users/<user_id>/posts/<post_id>")
class Like(Resource):
    @jwt_required
    @last_request_time
    @api_likes.doc(params={'user_id': 'UUID of the user.', 'post_id': 'UUID of the post.'},
                   body=like_fields, security='Access token',
                   responses={200: 'Successfully updated.', 201: 'Successfully created.'})
    def put(self, user_id, post_id):
        """Like-unlike post by the user.
        """
        put_data = request.get_json()

        from app.services.like_service import LikeService
        return LikeService.update_like(put_data, user_id, post_id)


@api_likes.route("/posts/<post_id>/number")
class LikeCount(Resource):
    @jwt_required
    @last_request_time
    @api_likes.doc(params={'post_id': 'UUID of the post.'}, security='Access token')
    def get(self, post_id):
        """Get number of likes for the post.
        """
        from app.services.like_service import LikeService
        return LikeService.count_number_of_likes_per_post(post_id)
