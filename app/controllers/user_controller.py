from flask_restplus import Namespace, Resource
from flask_jwt_extended import jwt_required

from app.services.utils_service import last_request_time


api_user = Namespace("users", description="User requests.")


@api_user.route("/")
class User(Resource):
    @jwt_required
    @last_request_time
    @api_user.doc(security='Access token')
    def get(self):
        """Get all users.
        """
        from app.services.user_service import UserService
        return UserService.get_all_users()


@api_user.route("/<user_id>/activity")
class UserActivity(Resource):
    @jwt_required
    @last_request_time
    @api_user.doc(params={'user_id': 'UUID of the user.'}, security='Access token',
                  responses={200: 'Success.', 404: 'User not found.'})
    def get(self, user_id):
        """Get user last login and last request time.
        """
        from app.services.user_service import UserService
        return UserService.get_user_activity_info(user_id)
