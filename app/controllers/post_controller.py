from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_jwt_extended import jwt_required

from app.services.utils_service import last_request_time


api_posts = Namespace("posts", description="Posts requests.")

post_fields = api_posts.model('Create post payload.', {
    "post_text": fields.String,
    "post_image": fields.String,
    "user_id": fields.String,
})


@api_posts.route("/")
class Post(Resource):
    @jwt_required
    @last_request_time
    @api_posts.doc(params={'user_id': 'Get posts of particular user.'}, security='Access token')
    def get(self):
        """Get all posts.
        """
        query_params = request.args

        from app.services.post_service import PostService
        return PostService.get_all(query_params)

    @jwt_required
    @last_request_time
    @api_posts.doc(body=post_fields, security='Access token',
                   responses={201: 'Successfully created.'})
    def post(self):
        """Create post.
        """
        post_data = request.get_json()

        from app.services.post_service import PostService
        return PostService.create(post_data)
